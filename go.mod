module gitlab.com/nakami2/tools

go 1.21

require (
	github.com/gabriel-vasile/mimetype v1.4.3
	github.com/go-playground/validator/v10 v10.15.5
	github.com/tealeg/xlsx v1.0.5
	github.com/volatiletech/null/v8 v8.1.2
	golang.org/x/exp v0.0.0-20231006140011-7918f672742d
	golang.org/x/net v0.17.0
)

require (
	github.com/friendsofgo/errors v0.9.2 // indirect
	github.com/go-playground/locales v0.14.1 // indirect
	github.com/go-playground/universal-translator v0.18.1 // indirect
	github.com/gofrs/uuid v4.4.0+incompatible // indirect
	github.com/leodido/go-urn v1.2.4 // indirect
	github.com/volatiletech/inflect v0.0.1 // indirect
	github.com/volatiletech/randomize v0.0.1 // indirect
	github.com/volatiletech/strmangle v0.0.5 // indirect
	golang.org/x/crypto v0.14.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/text v0.13.0 // indirect
	golang.org/x/xerrors v0.0.0-20231012003039-104605ab7028 // indirect
)
